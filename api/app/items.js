const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {

    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `items`', (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `items` WHERE `id` = ?',req.params.id, (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });
    });

    router.post('/', upload.single('image'), (req, res) => {
        const item = req.body;

        if (req.file) {
            item.image = req.file.filename;
        }

      connection.query('INSERT INTO `items` (`category_id`, `place_id`, `title`, `description`, `image`) VALUES (?, ?, ?, ?, ?)',
        [item.category_id, item.place_id, item.title, item.description, item.image],
        (error, results) => {
          if (error){
            console.log(error);
            res.status(500).send({error:'Database error'});
          }

          res.send({message: 'OK'});
        }
      );
    });

    router.delete('/:id', (req, res) => {//функция не прописана
      connection.query('DELETE FROM `items` WHERE `id` = ?',req.params.id, (error,results) =>{
        if (error){
          res.status(500).send({error:'Database error'});
        }
        res.send({message: 'OK'});
      });
    });

    router.put('/:id', upload.single('image'), (req, res) => {
        const item = req.body;

        let qery = 'UPDATE `items` SET `category_id` = ?, `place_id` = ?, `title` = ?, `description` = ? WHERE `id` = ?';
        const val = [item.category_id, item.place_id, item.title, item.description, req.params.id];


        if (req.file) {
            item.image = req.file.filename;
            qery = 'UPDATE `items` SET `category_id` = ?, `place_id` = ?, `title` = ?, `description` = ?, `image` = ? WHERE `id` = ?';
            val.push(item.image)
        }

        connection.query(qery,val,
            (error, results) => {
                if (error){
                    console.log(error);
                    res.status(500).send({error:'Database error'});
                }

                res.send({message: 'OK'});
            }
        );
    });

    return router;
};






module.exports = createRouter;