const express = require('express');
const nanoid = require('nanoid');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `places`', (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `places` WHERE `id` = ?',req.params.id, (error,results) =>{
            if (error){
                res.status(500).send({error:'Database error'});
            }
            if (results[0]){
                res.send(results[0]);
            } else {
                res.status(404).send({error:'Item not found'});
            }
        });
    });

    router.post('/', (req, res) => {
        const place = req.body;
        connection.query('INSERT INTO `places` (`location`, `description`) VALUES (?, ?)',
        [place.location, place.description],
        (error, results) => {
          if (error){
            console.log(error);
            res.status(500).send({error:'Database error'});
          }

          res.send({message: 'OK'});
        }
        );
    });

    router.delete('/:id', (req, res) => {//функция не прописана
      connection.query('DELETE FROM `places` WHERE `id` = ?',req.params.id, (error,results) =>{
        if (error){
          res.status(500).send({error:'Database error'});
        }
        res.send({message: 'OK'});
      });
    });

    router.put('/:id', (req, res) => {
        const place = req.body;

        let qery = 'UPDATE `places` SET `location` = ?, `description` = ? WHERE `id` = ?';
        const val = [place.location, place.description, req.params.id];

        connection.query(qery,val,
            (error, results) => {
                if (error){
                    console.log(error);
                    res.status(500).send({error:'Database error'});
                }

                res.send({message: 'OK'});
            }
        );
    });

    return router
};





module.exports = createRouter;