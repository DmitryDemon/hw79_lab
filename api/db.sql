CREATE SCHEMA `accounting` DEFAULT CHARACTER SET utf8 ;

USE `accounting`;

CREATE TABLE `categories` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`title` VARCHAR(255) NOT NULL,
	`description` TEXT NULL
);

CREATE TABLE `places` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`location` VARCHAR(255) NOT NULL,
	`description` TEXT NULL
);

CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NULL,
  `place_id` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`category_id` ASC),
  INDEX `place_id_fk_idx` (`place_id` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `place_id_fk`
    FOREIGN KEY (`place_id`)
    REFERENCES `places` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);
    
INSERT INTO `categories` (`id`,`title`,`description`) 
VALUES
	(1,'Furniture','Set of mobile or built-in products for furnishing residential and public premises'),
	(2,'Сomputer equipment','Computers of various types'),
	(3,'Appliances','Mechanical appliances that perform some household functions'),
	(4,'Industrial equipment','Woodworking equipment'),
	(5,'Cars','Employee vehicles')
;
    
INSERT INTO `places` (`id`,`location`,`description`) 
VALUES
	(1,'office','Office is building, complex of buildings in which employees of the enterprise (company) workbjects on it'),
	(2,'server room',NULL),
	(3,'kitchen','Piece for cook'),
	(4,'joinery workshop',NULL),
	(5,'garage',NULL);
    
INSERT INTO `items` (`id`,`category_id`,`place_id`,`title`,`description`)
VALUES
	(1,1,1,'Chair','sit on it'),
	(2,2,2,'MacBook Pro 2018 MR9Q2','Very expensiv laptop'),
	(3,3,3,'Cooker','Gas with convection'),
	(4,4,4,'Surface gauge','woodworking equipment'),
	(5,5,5,'VW Multivan','good family car');
    
    